import logo from './logo.svg';
import './App.css';
import { Switch, Route } from 'react-router-dom';
import Home from './Home';
import Create from './Create';
import Edit from './Edit';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route path="/" component={Home} />
        <Route path="/edit/:_id" component={Edit} />
        <Route path="/create" component={Create} />
      </Switch>
    </div>
  );
}

export default App;
