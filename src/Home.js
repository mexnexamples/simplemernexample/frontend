import { useState, useEffect } from "react";
import List from "./List";
import Create from "./Create";

const Home = () => {

    const [ games, setGames ] = useState([]);
    const [ hiddenEdit, setHiddenEdit ] = useState(true);
    const [ hiddenCreate, setHiddenCreate ] = useState(true);
    const [ gameToCreate, setGameToCreate ] = useState({Name: "", Brand: "", Price: 0, Replayability: 50, Genre: "", BloodAndGore: false});

    const propertiesGames = ["Name", "Brand", "Price", "Replayability", "Genre", "BloodAndGore"];
    const propertiesAndTypesGames = [
        {propertyName: "Name", inputType: "text"}, 
        {propertyName: "Brand", inputType: "text"}, 
        {propertyName: "Price", inputType: "number"}, 
        {propertyName: "Replayability", inputType: "range"}, 
        {propertyName: "Genre", inputType: "text"}, 
        {propertyName: "BloodAndGore", inputType: "checkbox"}
    ]
    const styleContainer = { 
        zIndex: '9', 
        position: 'absolute', 
        margin: '0',
        top: '30%',
        bottom: '45%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        padding: '10px',
        backgroundColor: 'white',
        border: '1px solid #AAAAAA',
        borderRadius: '10px'
    };


    useEffect(() => {
        fetch("/api/games/all")
        .then((res) => res.json())
        .then((allGames) => {
            setGames(allGames);
        });
    }, [])

    const popUpToCreate = () => {
        setGameToCreate({Name: "", Brand: "", Price: 0, Replayability: 50, Genre: "", BloodAndGore: false});
        setHiddenCreate(!hiddenCreate);
        setHiddenEdit(true);
    }

    const sendNewGameToBackend = () => {
        const createUrl = "/api/game/";
        fetch(createUrl, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(gameToCreate)
        })
        .then((res) => res.json())
        .then((newGame) => {
            fetch("/api/games/all")
            .then((res) => res.json())
            .then((allGames) => {
                setGames(allGames);
            });
        });
    };

    return (
        <div>
            <div style={{...styleContainer, visibility: (hiddenCreate ? "hidden" : "visible") }}>
                <Create 
                    game={gameToCreate}
                    setGame={setGameToCreate}
                    propertiesAndTypesGames={propertiesAndTypesGames}
                    sendToBackend={sendNewGameToBackend}
                    setHidden={setHiddenCreate}
                />
            </div>
            <button style={{margin: '20px'}} onClick={() => popUpToCreate()}>
                Add a game
            </button>
            <List 
                games={games}
                setGames={setGames}
                propertiesGames={propertiesGames}
                propertiesAndTypesGames={propertiesAndTypesGames}
                hiddenEdit={hiddenEdit}
                setHiddenEdit={setHiddenEdit}
                setHiddenCreate={setHiddenCreate}
            />
        </div>
    )
};

export default Home;