import { useState, useEffect } from "react";
import Edit from "./Edit";

const List = ({ games, setGames, hiddenEdit, setHiddenEdit, setHiddenCreate, propertiesGames, propertiesAndTypesGames }) => {

    const [ gameToEdit, setGameToEdit ] = useState({Name: "", Brand: "", Price: 0, Replayability: 50, Genre: "", BloodAndGore: false});
    const styleContainer = { 
        zIndex: '9', 
        position: 'absolute', 
        margin: '0',
        top: '30%',
        bottom: '45%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        padding: '10px',
        backgroundColor: 'white',
        border: '1px solid #AAAAAA',
        borderRadius: '10px'
    };
    const styleTable = { marginLeft: 'auto', marginRight: 'auto' };


    const popUpToEdit = (game) => {
        setHiddenCreate(true);
        setHiddenEdit(false);
        setGameToEdit(game);
    }

    const sendEditedGameToBackend = (editedGame) => {
        const editUrl = "/api/game/" + editedGame._id;
        console.log(editedGame);
        console.log(JSON.stringify(editedGame))
        fetch(editUrl, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({...editedGame})
        })
        .then(res => res.json())
        .then(returnedEditedGame => {
            fetch("/api/games/all")
            .then((res) => res.json())
            .then((allGames) => {
                setGames(allGames);
            })
            setHiddenEdit(true);
        })
    }

    const deleteGame = (id) => {
        setHiddenCreate(true);
        setHiddenEdit(true);

        const deleteUrl = "/api/game/" + id;
        fetch(deleteUrl, {
            method: "DELETE"
        })
        .then(res => res.json())
        .then(deletedGame => {
            const gamesWithoutDeletedOne = games.filter(game => {
                if (game._id == id) { return false };
                return true;
            });
            setGames(gamesWithoutDeletedOne);
        })
    }

    return (
        <div>
            <div style={{...styleContainer, visibility: (hiddenEdit ? "hidden" : "visible") }}>
                <Edit
                    game={gameToEdit}
                    setGame={setGameToEdit}
                    propertiesAndTypesGames={propertiesAndTypesGames}
                    sendToBackend={() => sendEditedGameToBackend(gameToEdit)}
                    setHidden={setHiddenEdit}
                />
            </div>
            <table style={styleTable}>
                <thead>
                    <tr>
                        { propertiesGames.map(property => (
                            <th key={property}>
                                {property}
                            </th>
                        )) }
                    </tr>
                </thead>
                <tbody>
                    {games.map(game => (
                        <tr key={game._id + "tr"}>
                            { propertiesAndTypesGames.map(propAndType => (
                                <td key={propAndType.propertyName}>
                                    {game[propAndType.propertyName]}
                                </td>
                            )) }
                            <td>
                                <button onClick={() => popUpToEdit(game)}>
                                    Edit
                                </button>
                            </td>
                            <td>
                                <button onClick={() => deleteGame(game._id)}>
                                    Delete
                                </button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
};

export default List;